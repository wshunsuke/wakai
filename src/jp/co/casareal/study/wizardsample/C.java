package jp.co.casareal.study.wizardsample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class C extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.c);
	}

	public void gotoROOT(View v) {
		Intent intent = new Intent(getApplicationContext(), ROOT.class);
		startActivity(intent);
	}
}