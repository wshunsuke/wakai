package jp.co.casareal.study.wizardsample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class B extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.b);
	}

	public void gotoC(View v) {
		Intent intent = new Intent(getApplicationContext(), C.class);
		startActivity(intent);
	}
}