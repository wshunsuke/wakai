package jp.co.casareal.study.wizardsample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ROOT extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root);
	}

	public void gotoA(View v) {
		Intent intent = new Intent(getApplicationContext(), A.class);
		startActivity(intent);
	}
}