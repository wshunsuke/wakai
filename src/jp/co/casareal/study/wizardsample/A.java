package jp.co.casareal.study.wizardsample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class A extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a);
	}

	public void gotoB(View v) {
		Intent intent = new Intent(getApplicationContext(), B.class);
		startActivity(intent);
	}
}